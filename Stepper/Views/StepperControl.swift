//
//  StepperControl.swift
//  THDConsumer
//
//  Created by Joy Tao on 10/23/17.
//  Copyright © 2017 The Home Depot. All rights reserved.
//

import Foundation
import UIKit


@objc
protocol StepperControlDelegate {
    
    func updateValueBy(add:Int, minus:Int)
    func enableAddToCartButton(enabled: Bool)
    
}

@IBDesignable
class StepperControl: UIView, UITextFieldDelegate {
    
    enum StepperType: Int {
        case addition = 1
        case subtraction = -1
        case other = 0
    }
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var resultField: UITextField!
    
    @IBInspectable var nibName:String?
    
    var stepperControl: StepperControl?
    var isValidQuantity: Bool = true
    
    
    var minimun:Int = 0
    var maximun:Int = 0
    var step: Int = 0
    
    var quantityToAdd: Int{
        guard let control = self.stepperControl else {
            return 1
        }
        
        return Int(control.resultField.text!)!
    }
    
   @objc weak var delegate: StepperControlDelegate?
    
    // Internal
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    func xibSetup() {
        
        guard let _self: StepperControl = loadFromNib() else {
            return
        }
        
        _self.frame = bounds
        _self.autoresizingMask  = [.flexibleWidth, .flexibleHeight]
        addSubview(_self)
        
        self.stepperControl = _self
        self.stepperControl!.uiSetup()
        
    }
    
    func loadFromNib () -> StepperControl? {
        guard let nibName = nibName else { return nil}
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        return nib.instantiate(withOwner: self, options: nil).first as? StepperControl
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        stepperControl?.prepareForInterfaceBuilder()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "StepperControl", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    func uiSetup(){
        
        self.addButton.layer.borderColor = UIColor.gray.cgColor
        self.addButton.layer.borderWidth = 1
        self.addButton.touchAreaEdgeInsets = UIEdgeInsetsMake(-20, -14, 20, -5)
        
        self.minusButton.layer.borderColor = UIColor.gray.cgColor
        self.minusButton.layer.borderWidth = 1
        self.addButton.touchAreaEdgeInsets = UIEdgeInsetsMake(-20, -5, -20, -14)

        self.resultField.layer.borderColor = UIColor.gray.cgColor
        self.resultField.layer.borderWidth = 1
        self.resultField.layer.masksToBounds = true
        self.resultField.keyboardType = .numberPad
        self.resultField.keyboardAppearance = .dark
        self.resultField.setQuantityInputAccessoryView(target:self, selector: #selector(doneEditing))
        self.resultField.delegate = self
        self.resultField.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        

    }
    
    // Public
    func configure(value:Int, minVal: Int, maxVal: Int, step: Int = 1, delegate: StepperControlDelegate? = nil) {
        
        guard let control = self.stepperControl else {
            return
        }
        
        control.minimun = minVal
        control.maximun = maxVal
        control.step = step
        
        control.minusButton.tag = StepperType.subtraction.rawValue
        control.minusButton.isUserInteractionEnabled = (value <= control.minimun) ? false : true
        
        control.addButton.tag = StepperType.addition.rawValue
        control.addButton.isUserInteractionEnabled = (value >= control.maximun) ? false : true
        
        control.resultField.text = "\(value)"
        
        control.delegate = delegate
        
    }
    
    func reset() {
        guard let control = self.stepperControl else {
            return
        }
        
        control.isValidQuantity = true
        
        control.resultField.text = "\(control.minimun)"
        control.minusButton.isUserInteractionEnabled = true
        control.addButton.isUserInteractionEnabled = true

    }
    
    
    @IBAction func stepperButtonDidTapped(sender: UIButton) {
        
        var current:Int = Int(resultField.text!)!
        if (sender.tag == StepperType.addition.rawValue) {
            
            current = current + step

        }
        else if (sender.tag == StepperType.subtraction.rawValue) {
            
            current = current - step
        }
        self.resultField.text = "\(current)"
        self.addButton.isUserInteractionEnabled = (current >= self.maximun) ? false : true
        self.minusButton.isUserInteractionEnabled = (current <= self.minimun) ? false : true
        
        if let _ = delegate {
            if (sender.tag == StepperType.addition.rawValue)  {
                delegate!.updateValueBy(add: 1 * step, minus: 0)
            }
            else if (sender.tag == StepperType.subtraction.rawValue) {
                delegate!.updateValueBy(add: 0, minus: 1 * step)
            }
        }
    }
    @objc func textFieldDidChange() {
        var enabled: Bool = true
        if let textString: String = self.resultField.text {
            if (textString.count == 0) {
                self.addButton.isUserInteractionEnabled = false
                self.minusButton.isUserInteractionEnabled = false
                
                if let stepperDelegate = self.delegate {
                    stepperDelegate.enableAddToCartButton(enabled: false)
                }
            }
            else {
                let isNumeric: Bool = textString.isNumeric()
                if (isNumeric == false) {
                    enabled = false
                    self.isValidQuantity = false

                }
                else {
                    let quantity:Int = Int(textString)!
                    
                    
                    if (quantity <= 0) {
                        enabled = false
                        self.isValidQuantity = false

                    }
                    else {
                        enabled = true
                        self.isValidQuantity = true

                    }
                }
                self.addButton.isUserInteractionEnabled = enabled
                self.minusButton.isUserInteractionEnabled = enabled
                
                if let stepperDelegate = self.delegate {
                    stepperDelegate.enableAddToCartButton(enabled: enabled)
                    
                }
            }
            
        }
        else {
            self.isValidQuantity = false

            if let stepperDelegate = self.delegate {
                stepperDelegate.enableAddToCartButton(enabled: false)
                
            }
        }
    }
    
    @objc func doneEditing() {
        
        self.resultField.resignFirstResponder()
        if (self.isValidQuantity == false) {
            self.addButton.isUserInteractionEnabled = true
            self.minusButton.isUserInteractionEnabled = true
            self.resultField.text = "\(self.minimun)"
            
            if let stepperDelegate = self.delegate {
                stepperDelegate.enableAddToCartButton(enabled: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return (textField.text?.count)! + (string.count - range.length) <= 3
    }
}

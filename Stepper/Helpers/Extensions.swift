//
//  Extensions.swift
//  Stepper
//
//  Created by Joy Tao on 12/14/17.
//  Copyright © 2017 Joy Tao. All rights reserved.
//

import Foundation
import UIKit

// UITextField Extensions
extension UITextField {
    
    func setQuantityInputAccessoryView(target: Any?, selector :Selector?) {
        
        let flex: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: target, action: selector)
        let accessoryView = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 50))
        accessoryView.barStyle = .blackTranslucent
        accessoryView.items = [flex, done]
        accessoryView.sizeToFit()
        
        self.inputAccessoryView = accessoryView
    }
}

extension String {
    
    func isNumeric() -> Bool {
        let noDigits: NSCharacterSet = (NSCharacterSet.decimalDigits).inverted as NSCharacterSet
        if (self.rangeOfCharacter(from: noDigits as CharacterSet)) == nil {
            return true
        }
        return false
    }
}

// UIButton Extensions
private var pTouchAreaEdgeInsets: UIEdgeInsets = .zero
extension UIButton {
    var touchAreaEdgeInsets: UIEdgeInsets {
        get {
            if let value = objc_getAssociatedObject(self, &pTouchAreaEdgeInsets) as? NSValue {
                var edgeInsets: UIEdgeInsets = .zero
                value.getValue(&edgeInsets)
                return edgeInsets
            }
            else {
                return .zero
            }
        }
        set(newValue) {
            var newValueCopy = newValue
            let objCType = NSValue(uiEdgeInsets: .zero).objCType
            let value = NSValue(&newValueCopy, withObjCType: objCType)
            objc_setAssociatedObject(self, &pTouchAreaEdgeInsets, value, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if UIEdgeInsetsEqualToEdgeInsets(self.touchAreaEdgeInsets, .zero) || !self.isEnabled || self.isHidden {
            return super.point(inside: point, with: event)
        }
        
        let relativeFrame = self.bounds
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, self.touchAreaEdgeInsets)
        
        return hitFrame.contains(point)
    }
}
